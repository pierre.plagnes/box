using ppl.Crosscutting.Constants;

namespace ppl.Crosscutting.Exceptions {
    public class EmailNotFoundException : BaseException {
        public EmailNotFoundException() : base(ErrorConstants.EmailNotFoundType, "Email address not registered")
        {
        }
    }
}
