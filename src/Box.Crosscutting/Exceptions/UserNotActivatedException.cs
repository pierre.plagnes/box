using System.Security.Authentication;

namespace ppl.Crosscutting.Exceptions {
    public class UserNotActivatedException : AuthenticationException {
        public UserNotActivatedException(string message) : base(message)
        {
        }
    }
}
