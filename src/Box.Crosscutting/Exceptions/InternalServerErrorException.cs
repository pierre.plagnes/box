using ppl.Crosscutting.Constants;

namespace ppl.Crosscutting.Exceptions {
    public class InternalServerErrorException : BaseException {
        public InternalServerErrorException(string message) : base(ErrorConstants.DefaultType, message)
        {
        }
    }
}
