using System.Security.Authentication;

namespace ppl.Crosscutting.Exceptions {
    public class UsernameNotFoundException : AuthenticationException {
        public UsernameNotFoundException(string message) : base(message)
        {
        }
    }
}
